## overview

## network drivers
> bridge: Nếu không được chỉ định, Bridge thường được sử dụng khi ứng dụng run với container độc lập cần communicate với nhau
- bridge tầng Link Layer
![](https://blogs.bmc.com/wp-content/uploads/2018/06/osi-model-7-layers-1024x734.jpg)
- bridge cho phép container connect với nhau thông qua bridge network
- bridge áp dụng cho các container chạy trên cùng 1 deamon, để connect container thuộc các deamon khác nhau cần dùng ***overlay network***
  
## khác nhau user-defined và default bridge
- **User-definded bridges cung cấp tốt nhất sự cô lập và khả năng tương tác giữa container**
- các ứng dụng connect dễ dàng với nhau nếu chúng cùng kết nối với mạng bridge, do các các container có thể thấy các port của nhau mà không có port nào vô ý hiển thị ra bên ngoài
- một ứng dụng web front-end và database back-end. outside world cần truy cập và front-end (port 80), back-end chỉ cần truy cập database port mà không cần open port outsite world. Nếu sử dụng User-defind chỉ cần open port cho web font-end.
- nếu default-bridge cần open 2 port db and web. **-p**
- **user-defined bridges cung cấp phân giải DNS giữa các container**
- contianers default bridge network chỉ có thể truy cập thông qua địa chỉ IP nếu không dùng **--link**, còn dối với on user-defined bridge network container có thể phân giả địa chỉ thông qua name or alias của container
- **container có thể được attached và detached từ user-defined networks một cách nhanh chóng**
- trong lifetime của container, có thể connect or disconnect từ user-definded networks nhanh chóng. để remove container từ default bridge network. bạn cần dừng container và tạo lại nó với các tùy chọn khác.
- 