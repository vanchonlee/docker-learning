## 1. docker overview
- docker la một nền tảng mở cho việc phát triển, shipping and running applications.
- docker cho phép tách ròi ứng dụng với infrastrucure giúp phân phối ứng dụng nhanh chóng
- với docker có thể  shipping, testing, and deploying code cách nhanh chóng
## 2. docker flatform
- docker có thể chạy ứng dụng trong mội environment độc lập được gọi là container
- do không cần các thành phần ảo hóa không cần thiết vì vậy ta có thể run nhiều container trên cùng một host hơn so với máy ảo với cùng tổ hợp phần cứng
## 3. docker engine
![](https://docs.docker.com/engine/images/engine-components-flow.png)

## 4. docker dùng để làm gì?
- docker cung cấp môi trường được chuẩn hóa cho các nhà phát triển, docker thích hợp cho CI/CD (tích hợp và phân phối liên tục)
- ví dụ:
  1. dev viết code trên local và chia sẻ với member thông qua docker container
  2. sử dụng docker đưa ứng dụng môi trường test (tự động hoặc thủ công)
  3. khi có bug họ fix và deploy lại môi trường test và tiếp tục kiểm tra
  4. khi test hoàn thành deploy lên production
## 5. triển khai và nhân rộng
- dễ dàng triển khai và nhân rộng
- docker rất nhẹ và nhanh vì thế giảm chi phí cho các giải pháp ảo hóa
## 6. kiến trúc
- docker sử dụng kiến trúc client-server
- docker client giao tiếp docker deamon sử dụng REST API thông qua UNIX dockets or network interface
-  deamon thực hiện công việc nặng đòi hỏi thời gian và chi phí tài nguyên cao như
   -  running
   -  building
   -  distributing
## 7. docker deamon
- docker deamon listens Docker API quản lý các docker object như
  - image
  - containers
  - networks
  - volumes
## 8. docker registries
- giống như git docker registry là nới lưu trữ các image
- pull để lấy về và push để đưa lên
- có thể tạo registry local

## 9. image
- image chỉ đọc và là nơi định nghĩa các chỉ dẫn giúp docker tạo ra container
- image này thường tạo từ image khác
- các chỉ dẫn được lưu trong Dockerfile
- mỗi instruction là một layer trong image, khi instruction nào đó thay đổi thì chỉ có layer đó trong image thay đổi khi rebuild
- đó chính là lý do giúp image nhanh, nhỏ và nhẹ khi so sánh với các nền tảng ảo hóa khác
## 10. container
- container là thể hiện của image khi được chạy
- có thể create, start, stop, move or delete container sử dụng Docker API or CLI
- có thể connect container đến 1 hoặc nhiều networks, lưu trữ dữ liệu của nó, tạo image từ trạng thái hiện tại của nó
## 11. service
- service cho phép scale containers thông qua nhiều docker deamons
- tất cả làm việc cùng nhau như một swarm với nhiều managers và worker.
- mỗi thành viên của swarm là một docker deamon và tất cả docker deamon giao tiếp thông qua docker API.
- service cho phép định nghĩa trạng thái, như số lượng replicas ở bất kỳ thời điểm nào
- mặc định thì tất cả service được load-balanced với tất cả các node 
## 12. docker được viết bằng Go và sử dụng một số tính nằng linux kernel
## 13. namespaces
- như lập trình namespaces cung cấp không gian để đặt trên để khỏi bị trùng
- docker sử dụng namespaces để cung cấp không gian làm việc độc lập cho container
- khi run ứng dụng docker tạo ra một namspaces cho container
- namespace cung cấp layer of isolation. tất các state được giới hạn trong namespace
- docker engine sử dụng namespace như linux
  - pid: process ID
  - net: managing network interfaces (network)
  - ipc: IPC réource
  - mnt: filesystem mount
  - uts: timeshararing system
## 14. Control group
- giới hạn tài nguyên cho một ứng dụng cụ thể
## 15. union file systems
- union file system hệ thống tệp hoạt động dựa trên các lớp làm chúng nhanh và nhẹ.

> ### *Giữ image small*
> không buil image từ các image không cần thiết
> nhiều image giống nhau thì tốt nhất nên tự build cái tương tự
> image phải sạch sẽ dễ đọc dễ debug
> luôn luôn thêm tag như: **test**, **prod** để còn biết image đó dùng làm gì, không dùng tag được khởi tạo tự động

## 16. sử dụng swarm services khi có thể
> - có thể sử dụng swarm service để thiết kê ứng dụng nhằm đảm bảo scale thật tốt
> - thậm chí có thể sử dụng cho container đọc lập nói chúng dùng được thì cứ dùng
> - network and volumes có thể connect and disconnect từ swarm service. docker điều khiển việc redeploy service container mà không gây gián đoạn (nó thực hiện update)
> - các container độc lập phải dừng, xóa, tạo lại (nhược điểm so với service đó)
> - một số feature như **secrets**, **configs** chỉ có thể dùng cho service thay thì container độc lập (ưu điểm nwuax so với container)
> - sử dụng stack deploy để pulls image về thay gì docker pull, khi đó quá trình sẽ không pull từ các node hỏng


## 17. sừ dụng CI/CD for testing and deployment
- trước khi đưa vào production đảm bảo tất cả team sign to images điều nyaf giúp đảm bảo image được kiểm tra và tình trạng tốt nhất.
> development sử dụng bind mount cho container truy cập source | pro dùng volumes
> sử dụng docker ee khi có thể cho production
> luôn luôn sử dụng NTP cliet trên docker và đồng bộ tất cả not trên swarm với NTP

